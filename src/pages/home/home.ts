import { Component } from "@angular/core";
import { NavController } from "ionic-angular";

import { ApiProvider } from "../../providers/api/api";

import * as WP from "wordpress-rest-api";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  allPosts: any;
  constructor(public navCtrl: NavController, public api: ApiProvider) {
    this.api.get().subscribe(data => {
      console.log(data);
    });
  }
}
