import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import { HomePage } from "../home/home";

import { ApiProvider } from "../../providers/api/api";
import * as WP from "wordpress-rest-api";

@Component({
  selector: "page-menu",
  templateUrl: "menu.html"
})
export class MenuPage {
  HomePage;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider
  ) {
    this.HomePage = HomePage;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MenuPage");
  }
}
