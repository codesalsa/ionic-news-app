import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class ApiProvider {
  private API_URL: string = "https://latestodishanews.com/wp-json/wp/v2/posts";
  public categories: any = [];

  constructor(public http: HttpClient) {}

  get(query: string = "") {
    return this.http.get(this.API_URL + query);
  }

  getCategories() {
    this.get("categories");
  }
}
